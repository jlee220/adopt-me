import { useEffect, useState } from "react";

const Counter = () => {
  const [state, setState] = useState({
    count1: 0,
    count2: 0,
    count3: 0,
  });

  useEffect(() => {
    const count1Click = (count1) => {
      setBuild((build) => ({
        ...build,
        count1,
      }));
    };
    const count2Click = (count2) => {
      setBuild((build) => ({
        ...build,
        count2,
      }));
    };
    const count3Click = (count3) => {
      setBuild((build) => ({
        ...build,
        count3,
      }));
    };
  });

  return (
    <>
      <div>
        {/* <button
              onChange={state.count1}
              value={state.count1 + 1}
              placeholder="Manufacturer Name"
              required
              name="name"
              id="name"
              className="form-control"
            /> */}
      </div>
      <button onClick=>{() => setState(state.count1 + 1}</button>
    </>
  );
};

export default Counter;
